import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WordsAppComponent } from './words-app/words-app.component';

const routes: Routes = [
  { path: 'app', component: WordsAppComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
