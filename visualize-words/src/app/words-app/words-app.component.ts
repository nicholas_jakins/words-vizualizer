import { Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WordsService } from '../words.service';
import { BusService } from '../bus.service';

@Component({
  selector: 'words-app',
  templateUrl: './words-app.component.html',
  styleUrls: ['./words-app.component.css']
})
export class WordsAppComponent {

  text: string = ''
  words: string[] = []
  wordInput: string = ''

  constructor(private wordsService: WordsService, public bus: BusService) { }

  clearText(): void {
    this.wordsService.deleteWords(this.bus.user).subscribe({
        next: data => {
          console.log('received response');
        },
        error: error => {
            console.error('There was an error!', error);
        }
    });
    this.text = ''
    this.words = []
  }

  addWord(): void {
    if (this.wordInput != '') {
      this.wordsService.submitWord(this.wordInput, this.bus.user).subscribe({
              next: data => {
                  this.words.push(this.wordInput)
                  this.wordInput = ''
                  this.setText()
              },
              error: error => {
                  console.error('There was an error!', error);
              }
          });
     }
  }

  setText(): void {
    this.wordsService
      .getWords(this.bus.user)
      .subscribe({
          next: data => {
            let obj = JSON.parse(JSON.stringify(data))
            this.text = obj.area
          },
          error: error => {
              console.error('There was an error!', error);
          }
      });
  }
}
