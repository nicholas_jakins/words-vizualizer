import { Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from './login.service';
import {Location} from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  showLogin: boolean = true
  userName: string = ''
  alertMessage: string = ''
  showAlert: boolean = false

  constructor(private loginService: LoginService, private router: Router) { }

  login(): void {
    this.loginService.submitUser(this.userName)
      .subscribe(resp => {
          this.showAlert = true
          if (resp.status == 201) {
            this.alertMessage = "New user added";
          } else if (resp.status == 200) {
            this.alertMessage = "User already exists";
          }
          this.showLogin = false;
          this.loginService.publishUser(this.userName)
          this.router.navigate(['app']);
        })
  }
}
