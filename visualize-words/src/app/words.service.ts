import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WordsService {

  hostname: string = "http://" + environment.apiHost + ":9090"
  url: string = "/api/words"
  headers: HttpHeaders = new HttpHeaders()

  constructor(private http: HttpClient) {}

  createAuthHeader(userName: string): HttpHeaders {
    const headerDict = { 'Authorization': 'Bearer ' + userName }
    return new HttpHeaders(headerDict);
  }

  submitWord(word: string, userName: string): Observable<string> {
    return this.http.put<any>(this.hostname + this.url, [word], { headers: this.createAuthHeader(userName) })
  }

  getWords(userName: string): Observable<string> {
    return this.http.get<any>(this.hostname + this.url, { headers: this.createAuthHeader(userName) })
  }

  deleteWords(userName: string): Observable<string> {
    return this.http.delete<any>(this.hostname + this.url, { headers: this.createAuthHeader(userName) })
  }
}
