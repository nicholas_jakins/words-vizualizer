import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BusService {

    user: string = '';

    add(userName: string) {
      this.user = userName;
    }

    clear() {
      this.user = '';
    }

}
