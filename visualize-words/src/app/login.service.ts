import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { environment } from './../environments/environment';
import { BusService } from './bus.service'


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  hostname: string = "http://" + environment.apiHost + ":9090"
  url: string = "/api/user"

  constructor(private http: HttpClient, public bus: BusService) { }

  submitUser(username: string): Observable<HttpResponse<any>> {
      return this.http.post<any>(this.hostname + this.url, username, {observe: 'response'});
  }

  publishUser(user: string): void {
    this.bus.add(user);
  }
}
