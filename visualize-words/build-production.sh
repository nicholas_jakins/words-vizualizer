#!/bin/sh
ng build --configuration production
docker build -t ntjakins/visualize-words:prod .
docker push ntjakins/visualize-words:prod
